//     LISTA ENCADEADA SIMPLES

#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include <locale.h>

using namespace std;

int main(){
    setlocale(LC_ALL, "Portuguese");

    struct NODE{
        int num;
        NODE *prox;
    };

    NODE *inicio = NULL;
    NODE *fim = NULL;

    NODE *aux;
    NODE *anterior;

    int op, numero, achou;

    do{
        system("cls");
        cout<<"\nMenu de OP��ES\n";
        cout<<"\n1 - Inserir no in�cio da lista";
        cout<<"\n2 - Inserir no fim da lista";
        cout<<"\n3 - Consultar toda a lista";
        cout<<"\n4 - Remover da lista";
        cout<<"\n5 - Esvaziar a lista";
        cout<<"\n6 - Sair";
        cout<<"\nDigite a sua op��o: ";
        cin>>op;

        if(op <1 || op>6){
            cout<<"Op��o Inv�lida\n";
            getch();
        }
        //Inserir no inicio
        if(op ==1){
            cout<<"\nDigite o n�mero a ser inserido no in�cio da lista: ";
            NODE *novo = new NODE();
            //cin>>*novo.num;
            cin>>novo->num;
            if(inicio == NULL){
                inicio = novo;
                fim = novo;
                fim->prox = NULL;
            }else{
                novo->prox=inicio;
                inicio=novo;
            }
            cout<<"Numero inserido no in�cio da lista";
            getch();
        }
        //Inserir no fim
        if(op == 2){
            cout<<"\nDigite o n�mero a ser inserido no fim da lista: ";
            NODE *novo =  new NODE();
            cin>>novo->num;
            if(inicio == NULL){
                inicio = novo;
                fim = novo;
                fim->prox = NULL;
            }
            else{
                fim->prox = novo;
                fim = novo;
                fim->prox = NULL;
            }
            cout<<"N�mero inserido no fim da lista";
            getch();
        }
        //Consultar  toda a lista
        if(op==3){
            if(inicio == NULL){
                cout<<"Lista  vazia\n";
                getch();
            }
            else{
                cout<<"\nConsultando toda a lista\n";
                aux = inicio;
                while(aux !=NULL){
                    cout<<aux->num<<" ";
                    aux = aux->prox;
                }
                getch();
            }
        }
        //Remover numero da lista
        if(op == 4){
            if(inicio == NULL){
                cout<<"Lista vazia\n";
                getch();
            }
            else{
                cout<<"\nDigite o numero a ser removido: ";
                cin>>numero;
                aux = inicio;
                anterior = NULL;
                achou = 0;

                while(aux != NULL){
                    if(aux->num == numero){
                        achou++;
                        if(aux == inicio){
                            inicio = aux->prox;
                            delete(aux);
                            aux = inicio;
                        }else if(aux == fim){
                            anterior->prox = NULL;
                            fim = anterior;
                            delete(aux);
                            aux = NULL;
                        }
                        else{
                            anterior->prox = aux->prox;
                            delete(aux);
                            aux  = anterior->prox;
                        }
                    }
                    else{
                        anterior  = aux;
                        aux= aux->prox;
                    }
                }
                if(achou == 0){
                    cout<<"N�mero n�o encontrado\n";
                    getch();
                }
                else if(achou == 1){
                    cout<<"N�mero removido 1 vez\n";
                    getch();
                }
                else{
                    cout<<"N�mero removido "<<achou<<" vezes\n";
                    getch();
                }
            }
        }
        //Limpar a lista
        if(op == 5){
            if(inicio == NULL){
                cout<<"Lista vazia\n";
                getch();
            }
            else{
                aux = inicio;
                while(aux != NULL){
                    inicio = inicio->prox;
                    delete(aux);
                    aux =  inicio;
                }
                cout<<"Lista esvaziada\n";
                getch();
            }
        }

    }while(op != 6);

        system("pause");
        return 0;
}

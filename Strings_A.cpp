#include <iostream>
#include <locale.h>
#include <stdio.h>
#include <iomanip>
#include <cstring>

using namespace std;

void strdel(char str[], int pos);

int main(){
	setlocale(LC_ALL, "Portuguese");
	//Inicializa��o de strings
	/*
	char nome[] = {'A','B','C','D','\0'};
	char nome2[]= "ABCD";
	
	cout<<nome<<endl;
	cout<<nome2<<endl;*/
	
	//Fun��es gets e setw
	/*
	char nome[80];
	cout<<"Digite seu nome: \n";
	//cin>>nome;
	//cin>>setw(2)>>nome;
	gets(nome);
	cout<<"Seu nome �: "<<nome<<endl;*/
	
	//Fun��o strlen tamanho das strings
	/*
	char nome[80];
	cout<<"Digite seu nome: \n";
	gets(nome);
	int len = strlen(nome);
	cout<<len;*/
	
	//Substrings a partir do valor digitado
	//char nome[] = "Gilberto";
	//cout<<(nome+3)<<endl;
	
	//Caixa alta e baixa
	/*
	char letra = 'A';
	char letra2 = 'a';
	
	letra = tolower(letra);
	letra2 = toupper(letra2);
	
	cout<<letra<<endl;
	cout<<letra2<<endl;*/
	
	//Fun��o strcat de concatena��o
	/*
	char saudacao[100]="Sauda��es, ";
	char nome[80];
	cout<<"Digite seu nome: \n";
	gets(nome);
	strcat(saudacao, nome);
	cout<<saudacao;*/
	
	//Compara��o de strings com strcmp
	/*
	char resposta[] = "BRANCO";
	char resp[40];
	
	cout<<"Qual a cor do cavalo branco de Napole�o?";
	gets(resp);
	
	while(strcmp(resp, resposta)!=0){
	//while(resp != resposta){
		cout<<"Resposta errada!Tente de novo.\n";
		gets(resp);
	}
	cout<<"Correto!\n";*/
	
	//Sa�das de strcmp
	/*
	cout<<strcmp("A","A")<<endl;
	cout<<strcmp("A","B")<<endl;
	cout<<strcmp("B","A")<<endl;
	cout<<strcmp("C","A")<<endl;
	cout<<strcmp("casa","casas")<<endl;*/
	
	//Fun��o strcpy para copiar strings
	/*
	char tex[] = "Bolo";
	char newtex[40];
	
	strcpy(newtex, tex); // O segundo vai para o primeiro 
	cout<<"tex: "<<tex<<endl;
	cout<<"newtex: "<<newtex<<endl;*/
	
	//Fun��o customizada para deletar caracteres  strdel
	char tex[] = "Carrta";
	cout<<tex<<endl;
	strdel(tex, 2);
	cout<<tex<<endl;
}

void strdel(char str[], int pos){
	strcpy(str+pos, str+pos+1);
}

#include <iostream>

using namespace std;

//enum frutas{banana=10, maca=20, abacaxi=30, mexirica=40};
//CRIAR ENUM PARA DIFICULDADE {facil, medio, dificil}
enum dificuldade{
    easy=1, medium, hard //easy=0 , medium=1, hard=2
};

struct Inimigo{
    string nome;
    int hp;
    int dano;
    dificuldade df;

    void criar(string snome, int shp, int sdano, dificuldade sdf){
        nome = snome;
        hp = shp;
        dano = sdano;
        df = sdf;
    }

    void mostrar(){
        cout<<"Nome.......:"<<nome<<endl;
        cout<<"HP.........:"<<hp<<endl;
        cout<<"Dano.......:"<<dano<<endl;
        cout<<"Dificuldade:"<<df<<endl;
        cout<<"\n";
    }
};

int main(){
    //frutas f;
    //f = abacaxi;
    //cout<<f<<endl;
    //Inimigo inimigo1, inimigo2, inimigo3;// = {"Orc", 100, 30};
    Inimigo inimigos[3];

    inimigos[0].criar("Zumbi", 50, 10, easy);
    inimigos[1].criar("Orc", 100, 30, medium);
    inimigos[2].criar("Minotauro", 200, 70, hard);

    for(int i =0;i<3;i++){
       inimigos[i].mostrar();
    }

    /*
    inimigo1.criar("Zumbi", 50, 10);
    inimigo1.mostrar();

    inimigo2.criar("Orc", 100, 30);
    inimigo2.mostrar();

    inimigo3.criar("Minotauro", 200, 70);
    inimigo3.mostrar();*/

    //inimigo1.nome = "Orc";
    //inimigo1.hp = 100;
    //inimigo1.dano = 30;
    //cout<<"Nome:"<<inimigo1.nome<<endl;
    //cout<<"HP:  "<<inimigo1.hp<<endl;
    //cout<<"Dano:"<<inimigo1.dano<<endl;

    return 0;
}

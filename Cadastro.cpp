#include <iostream>
#include <locale.h>

using namespace std;

int main(){
	setlocale(LC_ALL, "Portuguese");
	
	string nome[3];//char[3] = {'a','n','a'} 
	int dado_int[3][2];
	int i,k;
	
	for(i=0;i<3;i++){
		cout<<"Digite o nome do cliente "<<i+1<<endl;
		cin>> nome[i];
		
		for(k=0;k<2;k++){
			if(k==0)
				cout<<"Digite a idade do cliente "<<i+1<<endl;
			else
				cout<<"Digite o cpf do cliente "<<i+1<<endl;

			cin>> dado_int[i][k];
		}
		cout<<'\n';
	}
	
	for(i=0;i<3;i++){
		cout<<"O cliente "<<nome[i]<<" tem "<<dado_int[i][0]<< 
		" anos e seu cpf �: "<<dado_int[i][1]<<endl;
	}
}

#include <iostream>

using namespace std;

void somar(float *num, float soma);

int main(){
	
	int num = 4;
	//cout<<num<<endl;
	//cout<<&num<<endl; // Operador & est� relacionado com endere�o de mem�ria
	
	int *pnum; // Deve ser do mesmo tipo da vari�vel que aponta
				//Armazena o endere�o da mem�ria de outra vari�vel
	
	pnum = &num; // apontando para outra  vari�vel
	
	*pnum = 5; //atribui��o de valor na vari�vel apontada
	
	//cout<<num<<endl;
	//cout<<&num<<endl; // mostra endere�o da mem�ria da vari�vel
	//cout<<*pnum<<endl; // mostra o valor apontado
	//cout<<pnum<<endl; // mostra o endere�o apontado
	
	/*
	int n[10];
	int *pn;
	
	//p = n;
	pn = &n[0];
	cout<<pn<<endl;
	
	pn = &n[1];
	cout<<pn<<endl;
	
	*(pn++);
	//pn = &n[2];
	cout<<pn<<endl;
	*(pn++);
	//pn = &n[3];
	cout<<pn<<endl;
	*(pn++);
	//pn = &n[4];
	cout<<pn<<endl;
	*/
	float numero = 5;
	somar(&numero, 20);
	cout<<numero<<endl;
}

void somar(float *num, float soma){
	*num+=soma;
}

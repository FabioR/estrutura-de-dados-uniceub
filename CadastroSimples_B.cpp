#include <iostream>
#include <locale.h>

using namespace std;

int main(){
	setlocale(LC_ALL, "Portuguese");
	
	string nomes[3];
	int dados_int[3][2];
	int i,j;
	
	for(i=0;i<3;i++){
		cout<<"Digite o nome do cliente "<<i+1<<endl;
		cin>>nomes[i];
		for(j=0;j<2;j++){
			cout<<"Digite o dado do cliente "<<i+1<<endl;
			cin>>dados_int[i][j];	
		}
	}
	
	for(i=0;i<3;i++){
		cout<<"O cliente "<<nomes[i]<<" tem "<<dados_int[i][0]<<" anos e seu cpf �: "
		<<dados_int[i][1]<<endl;
	}
}

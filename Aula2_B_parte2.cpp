#include <iostream>
#include <stdio.h>
#include <locale.h>

using namespace std;

int main(){
	setlocale(LC_ALL, "Portuguese");

	unsigned int idade;
	
	cout << "Digite a sua idade:"<<endl;
	cin >> idade;
	
	if(idade >= 18 && idade < 60)
		cout << "Adulto \n";
	else if(idade >=60)
		cout << "Idoso\n";
	else
		cout << "Menor de idade\n";
		
	cout << "FIM!";
	/*
	float numero = 10.456789;
	printf("Digite um numero: \n");
	scanf("%f", &numero);
	printf("%4.2f",numero);
	*/
	/*
	int valor1, valor2, resultado;
	cout << "Qual o primeiro  valor?";
	cin >> valor1;
	cout << "Qual o segundo  valor?";
	cin >> valor2;
	
	resultado = valor1+valor2;
	cout << "O resultado da soma e: " << resultado<<endl;
	*/
	/*
	//char nome[40];
	string nome;
	
	cout << "Digite o seu  nome: \n";
	cin >> nome;
	cout << "Bom dia, " << nome <<endl; 
	*/
}

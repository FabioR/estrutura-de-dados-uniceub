#include <iostream>
#include <queue>

using namespace std;

int main(){
    queue<string> pessoas;
    pessoas.push("Jose");
    pessoas.push("Maria");
    pessoas.push("Roberto");
    pessoas.push("Bruna");
    pessoas.push("Pericles");

    //cout<<pessoas.front()<<endl;
    //cout<<pessoas.back()<<endl;
    //cout<<pessoas.size()<<endl;
    //cout<<pessoas.empty();
    //pessoas.pop();
    //cout<<pessoas.front()<<endl;
    //cout<<pessoas.back()<<endl;
    //cout<<pessoas.size()<<endl;

    while(!pessoas.empty()){
        cout<<pessoas.front()<<endl;
        pessoas.pop();
    }

    return 0;
}

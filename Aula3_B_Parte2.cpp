#include <stdio.h>
#include <locale.h>
#include <iostream>
#include <math.h>

using namespace std;

int main(){
	setlocale(LC_ALL, "Portuguese");
	
	double num1, num2, result;
	char sinal;
	string op;
	
	cout <<"Digite uma opera��o \n";
	cin >> num1 >> sinal >> num2;
	
	switch(sinal){
		case '+':
			result = num1+num2;
			op = "soma";
			break;
		case '-':
			result = num1-num2;
			op = "subtra��o";
			break;
		case '*':
			result = num1*num2;
			op = "multiplica��o";
			break;
		case '/':
			result = num1/num2;
			op = "divis�o";
			break;
		default:
			cout <<"Opera��o inv�lida\n";
			break;
	}
	cout <<"O resultado da "<<op<<" � "<<result<<endl;
	
	//Receber um numero, receber um sinal de opera��o char(+, -, *, /)
	//Receber o segundo numero
	//Dependendo do sinal fazer o calculo equivalente ao sinal
	
}

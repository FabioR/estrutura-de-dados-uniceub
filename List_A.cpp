#include <iostream>
#include <list>

using namespace std;

int main(){

    list<string>armas, armas2;// = {"Ak47", "AWP"};
    list<string>::iterator it;

    armas.push_front("AK-47");
    armas.push_front("Colt");
    armas.push_front("AWP");
    armas.push_back("Desert Eagle");

    armas2.push_front("UMP-45");
    armas2.push_front("P90");
    armas2.push_front("AUG");

    armas.merge(armas2);

    int tamanho = armas.size();

    armas.sort();
    armas.reverse();

    it = armas.begin();
    advance(it,3);
    armas.insert(it, "M4A1");
    advance(it,2);
    cout<<*it<<endl;
    armas.erase(it);

    cout<<"Tamanho da lista 1: "<<armas.size()<<endl;
    cout<<"Tamanho da lista 2: "<<armas2.size()<<endl;
    cout<<"Lista 1  vazia? "<<armas.empty()<<endl;
    cout<<"Lista 2  vazia? "<<armas2.empty()<<endl;

    for(int i=0;i<tamanho;i++){
        cout<<armas.back()<<endl;
        armas.pop_back();
    }

    //armas.clear();
    //cout<<"Tamanho da lista 1: "<<armas.size()<<endl;

    //armas.push_front();
    //armas.push_back();
    //armas.front();
    //armas.back();
    //armas.pop_front();
    //armas.pop_back();
    //armas.size();
    //armas.sort();
    //armas.reverse();
    //armas.merge();
    //armas.empty();
    //armas.clear();
    return 0;
}

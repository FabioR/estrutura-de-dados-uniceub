#include <iostream>
#include <stdio.h>

using namespace std;

void mensagem(string msg, string msg2="Tudo bem agora");

int main(int argc, char *argv[]){
    cout<<argv[0]<<endl;
    cout<<argv[1]<<endl;

    mensagem("Perigo!", "Fogo!");

    /*
    int num;
    char nome[30];

    printf("Digite seu nome: ");
    scanf("%s",nome);
    printf("Digite sua idade: ");
    scanf("%i",&num);

    printf("Ola %s, seu numero e: %d\n", nome, num);
    */
    /*
        d,i = int
        s = char[]
        f  = float
        p  = ponteiros
        u = unsigned int (sem sinal -)
        x = hexacecimal
    */
    return 0;
}

void mensagem(string msg, string msg2){
    cout<<msg<<endl;
    cout<<msg2<<endl;
}

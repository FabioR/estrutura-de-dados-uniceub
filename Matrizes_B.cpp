#include <iostream>
#include <locale.h>

using namespace std;

int main(){
	setlocale(LC_ALL, "Portuguese");
	
	string nomes[3];
	int dados_int[3][2];
	int n,i;
	
	for(n=0;n<3;n++){
		cout<<"Digite o nome do cliente "<<n+1<<endl;
		cin>>nomes[n];
		for(i=0;i<2;i++){
			if(i==0){
				cout<<"Digite a idade do cliente "<<n+1<<endl;	
			}	
			else{
				cout<<"Digite o cpf do cliente "<<n+1<<endl;	
			}	
			cin>>dados_int[n][i];
		}
	}
	
	for(n=0;n<3;n++){
		cout<<"O cliente "<<nomes[n]<< " tem "<< dados_int[n][0]
		<<" anos e seu cpf �: "<<dados_int[n][1]<<endl;
	}
}

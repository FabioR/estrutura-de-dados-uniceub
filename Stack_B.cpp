#include <iostream>
#include <stack>
#include <array>

using namespace std;

string naipe(int n){
    switch(n){
        case 1:
            return "Ouros";
        case 2:
            return "Copas";
        case 3:
            return "Paus";
        case 4:
            return "Espadas";
    }
}

string valor(int value){
    switch(value){
        case 1:
            return "As";
        case 2:
            return "Dois";
        case 3:
            return "Tres";
        case 4:
            return "Quatro";
        case 5:
            return "Cinco";
        case 6:
            return "Seis";
        case 7:
            return "Sete";
        case 8:
            return "Oito";
        case 9:
            return "Nove";
        case 10:
            return "Dez";
        case 11:
            return "Valete";
        case 12:
            return "Dama";
        case 13:
            return "Rei";
    }

    return "Invalido";
}
int main(){
    stack<array<int, 2>> cartas;
    //cout<<"Tamanho do deck: "<<cartas.size()<<endl;

    cartas.push({1,4});
    cartas.push({2,7});
    cartas.push({4,12});

    for(int i =0;i<3;i++){
        array<int, 2> card;
        card = cartas.top();
        cout<<valor(card[1])<<" de "<<naipe(card[0])<<endl;
        cartas.pop();
    }

    //cout<<"Tamanho do deck: "<<cartas.size()<<endl;
    //cout<<"Carta do topo: "<<cartas.top()<<endl;

    //cartas.pop();

    //cout<<"\n\nTamanho do deck: "<<cartas.size()<<endl;
    //cout<<"Carta do topo: "<<cartas.top()<<endl;


    return 0;
}

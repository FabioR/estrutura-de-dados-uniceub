#include <iostream>
#include <list>

using namespace std;

int main(){
    list<string> armas;
    list<string>::iterator it;

    armas.push_front("Colt");
    armas.push_front("AWP");
    armas.push_front("UMP-45");
    armas.push_front("AUG");
    armas.push_front("P90");

    int tamanho;
    tamanho = armas.size();
    cout<<"Tamanho da lista: "<<tamanho<<endl;

    armas.sort();
    armas.reverse();

    cout<<"\n\n";

    it = armas.begin();
    advance(it, 3);
    armas.insert(it, "Desert Eagle");
    tamanho = armas.size();
    advance(it, -2);
    armas.erase(it);

    for(int i=0;i<tamanho;i++){
        cout<<armas.back()<<endl;
        armas.pop_back();
    }

    //nums.push_front();
    //nums.push_back();
    //nums.front();
    //num.back();
    //num.pop_front();
    //num.pop_back();
    //num.size();
    //num.sort();
    //num.reverse();
    //num.merge();
    //num.empty(); //retorna bool true se est� vazia
    //nums.insert();//utiliza iterador
    //nums.erase()//utiliza iterador

    return 0;
}

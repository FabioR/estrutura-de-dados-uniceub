#include <iostream>
#include <queue>

using namespace std;

int main(){
    queue<string> pessoas;// = {5,6,9,8,7};
    pessoas.push("Jose");
    pessoas.push("Maria");
    pessoas.push("Roberto");
    pessoas.push("Bruna");
    pessoas.push("Pericles");

    cout<<"Primeiro numero: "<<pessoas.front()<<endl;
    cout<<"Ultimo numero: "<<pessoas.back()<<endl;
    cout<<"Quantidade de numeros: "<<pessoas.size()<<endl;

    /*
    nums.pop();
    cout<<"\n\n";

    cout<<"Primeiro numero: "<<nums.front()<<endl;
    cout<<"Ultimo numero: "<<nums.back()<<endl;
    cout<<"Quantidade de numeros: "<<nums.size()<<endl;*/

    //cout<<nums.empty()<<endl;
    while(!pessoas.empty()){
        cout<<pessoas.front()<<endl;
        pessoas.pop();
    }

    //pessoas.push()
    //pessoas.front()
    //pessoas.back()
    //pessoas.pop()
    //pessoas.empty()

    return 0;
}

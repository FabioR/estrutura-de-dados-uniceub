#include <iostream>
#include <locale.h>
#include <iomanip>
#include <stdio.h>
#include <cstring>

using namespace std;

void strdel(char str[], int pos);

int main(){
	setlocale(LC_ALL,"Portuguese");
	
	//Inicializa��o de strings
	/*
	//char nome[] = {'A','B','C','D','\0'};
	char nome[] = "ABCD";
	cout<<nome<<endl;
	*/
	
	//Fun��o setw e gets
	/*
	char nome[80];
	cout<<"Digite o seu nome: \n";
	//cin>>setw(4)>>nome;//include iomanip
	gets(nome);
	cout<<"Bom dia: "<<nome<<endl;
	*/
	
	//Substrings a partir do valor definido
	//cout<<(nome+2)<<endl;	
	
	//Fun��o strlen tamanho da string
	/*
	char nome[80];
	cout<<"Digite o seu nome: \n";
	gets(nome);
	int len = strlen(nome);
	cout<<len;*/
	
	//Fun��o strcat para concatena��o
	/*
	char saudacao[100] = "Sauda��es, ";
	char nome[80];
	cout<<"Digite o seu nome: \n";
	gets(nome);
	strcat(saudacao, nome);
	cout<<saudacao<<endl;*/
	
	//Fun��o strcmp para comparar strings
	/*
	char resposta[] = "BRANCO";
	char resp[40];
	
	cout<<"Qual a cor do cavalo branco de Napole�o?:\n";
	gets(resp);
	
	while(strcmp(resp,resposta) != 0){
	//while(resp != resposta){
		cout<<"Resposta Errada! Tente de novo.\n";
		gets(resp);
	}
	cout<<"Correto!\n";*/
	
	//Sa�das de strcmp
	/*
	cout<<strcmp("A","A")<<endl;
	cout<<strcmp("A","B")<<endl;
	cout<<strcmp("B","A")<<endl;
	cout<<strcmp("C","A")<<endl;
	cout<<strcmp("casa","casas")<<endl;*/
	
	//Fun��o strcpy para copiar strings
	/*
	char tex[]="Bolo";
	char newtext[40];
	
	strcpy(newtext, tex);//A segunda vai para a primeira
	cout<<newtext<<endl;
	cout<<tex<<endl;*/
	
	//Fun��o customizada strdel para deletar caracteres
	//utilizando strcpy e substrings
	/*
	char str[] = "Carrta";
	cout<<str<<endl;
	strdel(str,2);
	cout<<str<<endl;*/
	
	//Fun��o tolower e toupper caixa baixa e alta
	/*
	char letra = 'AB';
	char letra2 = 'ab';
	letra = tolower(letra);
	letra = toupper(letra2);
	cout<<letra<<endl;
	cout<<letra2<<endl;*/
	
	//Fun��o printf print formatado
	//char nome[] = "Jo�o";
	//int idade = 41;
	
	//printf("Ol� %s, voc� tem %d anos", nome, idade);
}

void strdel(char str[], int pos){
	strcpy(str+pos, str+pos+1);
}

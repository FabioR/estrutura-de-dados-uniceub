#include <iostream>

using namespace std;

//enum Frutas{
    //maca=10, laranja=20, pera, abacaxi=40, mamao,limao
//};
enum Dificuldade{
    facil, medio, dificil
};

struct Inimigo{
    string nome;
    int hp;
    int dano;
    Dificuldade df;

    void criar(string snome, int shp, int sdano, Dificuldade sdf){
        nome = snome;
        hp = shp;
        dano = sdano;
        df = sdf;
    }

    void mostra(){
        cout<<"Nome..........:"<<nome<<endl;
        cout<<"Pontos de Vida:"<<hp<<endl;
        cout<<"Dano..........:"<<dano<<endl;
        cout<<"Dificuldade...:"<<df<<endl;
        cout<<"\n\n";
    }
};

int main(){
    Inimigo inimigos[3];

    inimigos[0].criar("Orc", 100,30, medio);
    inimigos[1].criar("Zumbi", 50,10, facil);
    inimigos[2].criar("Minotauro", 200,50, dificil);


    for(int i =0; i<3;i++){
        inimigos[i].mostra();
    }

    /*
    Inimigo inimigo1, inimigo2, inimigo3;

    inimigo1.criar("Orc", 100,30);
    inimigo2.criar("Zumbi", 50,10);
    inimigo3.criar("Minotauro", 200,50);

    inimigo1.mostra();
    inimigo2.mostra();
    inimigo3.mostra();*/
/*
    for(int i =0; i<3;i++){

    }*/
    //inimigo1 = {"Orc", 100,30};

    /*
    inimigo1.nome = "Orc";
    inimigo1.hp = 100;
    inimigo1.dano = 30;

    cout<<"Nome..........:"<<inimigo1.nome<<endl;
    cout<<"Pontos de Vida:"<<inimigo1.hp<<endl;
    cout<<"Dano..........:"<<inimigo1.dano<<endl;
    */
    //Frutas f;
    //f = pera;
    //cout<<f<<endl;
    return 0;
}

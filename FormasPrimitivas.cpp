#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>

void Desenhar(){
	// Limpa a janela de visualiza��o com a cor  
	// de fundo definida previamente
	glClear(GL_COLOR_BUFFER_BIT);
	
	glColor3f(0,0,1);
	
	float theta, raio;
	raio = 10;
	
	glBegin(GL_POLYGON);
		for(int i=0;i<360;i++){
			theta = i*3.142/180;
			glVertex2f(raio*cos(theta), raio*sin(theta));
		}
	glEnd();
	
	glFlush();
}

void Inicializar(){
	//Define a cor de fundo da janela de visualiza��o como branca
	glClearColor(1,1,1,1);
}

// Fun��o callback chamada quando o tamanho da janela � alterado 
void Ajustar(GLsizei w, GLsizei h)
{
	GLsizei largura, altura;

	// Evita a divisao por zero
	if(h == 0) h = 1;

	// Atualiza as vari�veis
	largura = w;
	altura = h;

	// Especifica as dimens�es da Viewport
	glViewport(0, 0, largura, altura);

	// Inicializa o sistema de coordenadas
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Estabelece a janela de sele��o (esquerda, direita, inferior, 
	// superior) mantendo a propor��o com a janela de visualiza��o
	if (largura <= altura) 
		gluOrtho2D (-40.0f, 40.0f, -40.0f*altura/largura, 40.0f*altura/largura);
	else 
		gluOrtho2D (-40.0f*largura/altura, 40.0f*largura/altura, -40.0f, 40.0f);
}

void Teclado(unsigned char key, int x, int y){
	if(key == 27)
		exit(0);
}

int main(){
	//Define o modo de opera��o do GLUT
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	
	//Especifica a posi��o inicial da janela GLUT
	glutInitWindowPosition(5,5);
	
	//Especifica o tamanho inicial da janela GLUT em pixels
	glutInitWindowSize(450,450);
	
	//Criar janela com t�tulo
	glutCreateWindow("Casa");
	
	//Registra a fun��o callback de desenho da janela  de visualiza��o
	glutDisplayFunc(Desenhar);
	
	// Registra a fun��o callback de redimensionamento da janela de visualiza��o
	glutReshapeFunc(Ajustar);
	
	//Registra a fun��o callback de inputs de teclado
	glutKeyboardFunc(Teclado);
	
	//Inicializa  alguns par�metros
	Inicializar();
	
	//Inicia o processamento e aguarda intera��es do usu�rio
	glutMainLoop();
	
	return 0;
}

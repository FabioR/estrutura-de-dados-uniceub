#include <iostream>
#include <locale.h>

using namespace std;

int main(){
	setlocale(LC_ALL, "Portuguese");
	
	int num[2][2];
	int resultado[2][2];
	
	int l,c;
	int maior;
	
	//Receber os elementos da matriz
	for(l=0;l<2;l++){
		for(c=0;c<2;c++){
			cout<<"Digite o elemento da linha "<<l+1<<", coluna "
			<<c+1<<": ";
			cin >> num[l][c];
		}
	}
	maior = num[0][0];
	
	//Verificar o maior deles
	for(l=0;l<2;l++){
		for(c=0;c<2;c++){
			if(num[l][c] > maior){
				maior = num[l][c];
			}
		}
	}
	//Atribuir os novos valores a uma outra matriz
	for(l=0;l<2;l++){
		for(c=0;c<2;c++){
			resultado[l][c]= num[l][c]*maior;
		}
	}
	cout<< "A Matriz resultante �:\n";
	
	//Imprimir na tela a nova matriz		
	for(l=0;l<2;l++){
		for(c=0;c<2;c++){
			cout<<resultado[l][c]<<'\t';
		}
		cout<<'\n';
	}
	/*
	num[0][0] = 1;
	num[0][1] = 2;
	num[0][2] = 3;
	num[0][3] = 4;
	
	num[1][0] = 5;
	num[1][1] = 6;
	num[1][2] = 7;
	num[1][3] = 8;
	
	num[2][0] = 9;
	num[2][1] = 10;
	num[2][2] = 11;
	num[2][3] = 12;*/
}

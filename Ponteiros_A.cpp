#include <iostream>

using namespace std;

void initvetor(float *v);
void somar(float *var, float valor);

int main(){
	/*
	int num = 4;
	
	//cout<<&num; // Operador & est� relacionado ao endere�o da mem�ria
	int *p; //Deve ser do mesmo tipo da vari�vel que aponta
			//Armazena o  endere�o da mem�ria  de outra vari�vel
			
	p = &num;
	
	cout<<num<<endl;
	cout<<&num<<endl;
	cout<<*p<<endl;
	cout<<p<<endl;
	
	*p = 5;
	cout<<endl<<num<<endl;
	cout<<*p<<endl;
	
	int vetor[10];
	int *pv;
	
	pv = &vetor[0];//4 bytes de memoria por int
	//v = vetor;
	cout<<pv<<endl;
	
	pv = &vetor[1];
	cout<<pv<<endl;
	
	
	*(pv+=1);
	cout<<pv<<endl;
	
	*(pv+=1);
	cout<<pv<<endl;
	
	*(pv+=1);
	cout<<pv<<endl;
	*/
	
	//float vetor[5];
	//initvetor(vetor);
	float numero = 0;
	somar(&numero, 15);
	
	cout<<numero<<endl;
	
	//for(int i=0;i<5;i++){
		//cout<<vetor[i]<<endl;
	//}
}

void somar(float *var, float valor){
	*var+=valor;
}

void initvetor(float *v){
	v[0] = 0;
	v[1] = 1;
	v[2] = 2;
	v[3] = 3;
	v[4] = 4;
}
